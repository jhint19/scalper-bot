## Scalper Bot
<p> Scalper Bot allows the user to insert or update their website information for the websites allowed on the bot. <p>

## Dependency
<p>Please make sure that you have the following dependencies installed:</p>

+ msvc-runtime==14.29.30133
+ Python Version: 3.10.0
+ mysql-connector==2.2.9
+ regex==2021.4.4
+ pyasn1==0.4.8
+ pycparser==2.20
+ pyparsing==2.4.7
+ pause==0.1.2
+ python-dateutil==2.7.3
+ selenium==3.14.0
+ six==1.11.0
+ urlib3==1.24.

+ Uses http client
+ Download MySQL following the instructions here: https://www.mysql.com/de/downloads/

#### If you get matplotlib error: This worked for me on Windows 10 (using the Anaconda prompt):
> pip uninstall matplotlib <p>
> pip install --upgrade matplotlib <p>


## Running the App:
+ run main.py make sure all dependencies are installed
+ input correct mysql password
+ All configurable variables are defined in main.py

## Configuring the MySQL server:
+ configurations are made in main.py in set_env_variables()
+ The MySQL password should be set to the users password that they set up when downloading MySQL for their machine.
+ By default the MySQL user is root and the MySQL host is localhost.
+ YOu may also change the database name in the same location

## Test cases 
+ Test cases are downloaded with the rest of the code.
+ Simply open the spreadsheets to view them.

