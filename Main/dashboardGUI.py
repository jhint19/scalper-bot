from tkinter import *
import tkinter as tk
from tkinter import ttk
import dashboard_controller
import loginlogout_controller
import payment_controller

# this class controls the graphical user interface of the dashboard window. Its methods include 
# createSearchbarFrame, createWatchlistPortfolioFrame,
# handleWatchlistEvent, handlePortfolioEvent, handleSearchbarEvent, closeWindow.
class DashboardGUI():
    def __init__(self, master, userObject):
        self.dashboardControllerObject = dashboard_controller.DashboardController(userObject)
        self.master = master
        self.userObject = userObject
        self.master.configure(background= "LightYellow")
        self.master.title("Dashboard")
        self.createMainFrame()

    '''
    Intent: creates the main frame for the dashboard GUI
    * Preconditions: master is connected to TKinter. 
    * createWatchlistPortfolioFrame and createSearchbarFrame have the appropriate GUI code to be called in this method.
    * Postconditions:
    * Post0. main frame for dashboard is created
    '''
    def createMainFrame(self):
        self.logo = Label(self.master, text="Scalper Bot",font='Helvetica 12',height = 6, width = 13,borderwidth=2, relief="solid", background='LightBlue1').grid(row=0,column=0, pady=5, padx=5)
        self.createButtonsFrame()
        self.logoutButton = Button(self.master,text="Log Out", command=lambda:self.handleLogoutEvent(), background='red').grid(row = 4,column=1,sticky="se")



    '''
    Intent: creates the frame that contains the watchlist and portfolio for the dashboard GUI
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. frame that contains the watchlist and portfolio is created
    '''
    def createButtonsFrame(self):
        self.buttonFrame = Frame(self.master, width = 450, height = 200,borderwidth=2, relief="sunken", background='LightBlue1').grid(row = 2,column=1)
        self.buyLabel = Button( self.buttonFrame, text="Buy",command=lambda:self.handleBuyEvent(), font='Helvetica 13 bold',borderwidth=1, relief="ridge", background='white')
        self.buyLabel.grid(row = 2,column=1,padx=30,pady=25,ipadx=5,ipady=5, sticky="nw")
        self.websiteLoginLabel = Button( self.buttonFrame, text="Website Login",command=lambda:self.handleWebsiteEvent(), font='Helvetica 13 bold',borderwidth=1, relief="ridge", background='white')
        self.websiteLoginLabel.grid(row=2, column=1,padx=30,pady=25, ipadx=5,ipady=5,sticky="sw")
        self.paymentLabel = Button( self.buttonFrame, text="Payment",command=lambda:self.handlePaymentEvent(), font='Helvetica 13 bold',borderwidth=1, relief="ridge", background='white')
        self.paymentLabel.grid(row = 2,column=1,padx=30,pady=25,ipadx=5,ipady=5, sticky="ne")
        self.purchaseHistoryLabel = Button( self.buttonFrame, text="Purchase History", font='Helvetica 13 bold',borderwidth=1, relief="ridge", background='white')
        self.purchaseHistoryLabel.grid(row=2, column=1,padx=30,pady=25, ipadx=5,ipady=5,sticky="se")



    '''
    Intent: handles the logic for the user clicking payment button.
    * Preconditions: 
    * Postconditions: 
    * Post0. changes are pushed to database, payment window is closed and dashboard window is opened.
    * Post1. Changes are not pushed to database beacause connection to database could not be estbalished.
    
    '''
    def handlePaymentEvent(self):
        self.master.destroy()
        self.dashboardControllerObject.create_payment_controller()
        
    '''
    Intent: handles the logic for the user clicking website button.
    * Preconditions: 
    * Postconditions: 
    * Post0. changes are pushed to database, payment window is closed and dashboard window is opened.
    * Post1. Changes are not pushed to database beacause connection to database could not be estbalished.
    
    '''
    def handleWebsiteEvent(self):
        self.master.destroy()
        self.dashboardControllerObject.create_website_controller()
        
    
    '''
    Intent: handles the logic for the user clicking payment button.
    * Preconditions: 
    * Postconditions: 
    * Post0. changes are pushed to database, payment window is closed and dashboard window is opened.
    * Post1. Changes are not pushed to database beacause connection to database could not be estbalished.
    
    '''
    def handleBuyEvent(self):
        self.master.destroy()
        self.dashboardControllerObject.create_buy_controller()

    '''
    Intent: handles the logic for the user clicking logout button.
    * Preconditions: 
    * Postconditions: 
    * Post0. changes are pushed to database, dashboard window is closed and login window is opened.
    * Post1. Changes are not pushed to database beacause connection to database could not be estbalished.
    
    '''
    def handleLogoutEvent(self):
        username = self.userObject.current_user_data[1]
        self.dashboardControllerObject.logOutPushChanges(username, self.userObject)

        self.closeWindow()
        self.dashboardControllerObject.openLoginGUI()
        
    
    '''
    Intent: close the dashboard window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the dashboard window
    '''
    def closeWindow(self):
        self.master.destroy()



