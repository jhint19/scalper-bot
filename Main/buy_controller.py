# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 11:25:29 2022

@author: Owner
"""

import buy_gui
import dashboard_controller
import popupGUI
from database_manager import DB
from tkinter import *
import loginlogout_controller as LoginController
from user import User
from popupGUI import PopUpGUI
from selenium.webdriver.common.by import By
from datetime import datetime


# this class controls the controller of the portfolio window. Its methods include 
# calculate_portfolio_value, calculate_percentage_change, call_user_object_to_remove_stock, 
# create_stock_controller_object, get_stock_price_yahoo_api_object, create_portfolio_GUI,
# CreateWatchListController, create_popup_GUI
class BuyController():
    def __init__(self, user_object):
        #passed in objects
        self.userObject = user_object
        self.databaseManagerObject = DB()
        self.dashboardControllerObject = dashboard_controller.DashboardController(self.userObject)
        self.buyGUI = None
        

            



        '''
        Intent: Creates User object by passing username parameter. Returns user object
        * Preconditions: 
        * Postconditions:
        * Post0. User object created and returned.
        * Post1. User object is returned as None.
        '''
    def createUserObject(self,username):
        self.currentUserData = self.setCurrentUserData(username)
        self.currentUserItems = self.setCurrentItemData(username)
        self.userObject = User(self.currentUserData, self.currentUserItems)
        return self.userObject
    
    '''
    Intent: 
    * Preconditions: 
    * PopUpGUI exists
    * Postconditions:
    * Post0. 
    * Post1. 
    '''

    def changeBuyProcessing(self,website, date, time, url,GUI,securityCodeEntered,size):
        """Call VerifySecurityCodeUsername if its correct reset password for user in DB.
        then creates login gui.
        Else error message pop up GUI."""
        if self.userObject.current_user_data[3] != int(securityCodeEntered):
            popupGUI = PopUpGUI("Security Code Entered is incorrect")
            popupGUI.createPopUp()
        else:
            if website == "Footlocker":
                self.footlockerProcessing(website, date, time, url,GUI,size)
        
    def footlockerProcessing(self,website, date, time, url,GUI,sizeChosen):
        wUsername = 0
        wPassword = 0
        for x in self.databaseManagerObject.getDatabaseWebsiteData():
            if website in x:
                 if int(self.userObject.current_user_data[0]) ==x[2]:
                        wUsername=x[3] 
                        wPassword=x[4]
        if wUsername ==0  or wPassword ==0:   
            popupGUI = PopUpGUI("After checking our database, you do not have a login saved for this website!")
            popupGUI.createPopUp()  
        else:
        #check if this is the correct month to run the code
            '''
            now = datetime.now()
            while int(date[0]) != int(now.strftime("%m")):
                now = datetime.now()
        #check if today is the day to run the code
            now = datetime.now()
            while int(date[2:4])!=int(now.strftime("%d")):
                now = datetime.now()
        #check if current time is the time to run the code
            now = datetime.now()
            current_time = now.strftime("%H:%M")
            while current_time !=time:
                now = datetime.now()
                current_time = now.strftime("%H:%M")
            '''
            from selenium import webdriver as wd
            import time
            wd = wd.Chrome()
              
            wd.get(url)
            element = wd.find_element(By.TAG_NAME, 'div')

                # Get all the elements available with tag name 'p'
            elements = element.find_elements(By.TAG_NAME, 'button')
            for e in elements:
                if (e.text) == sizeChosen:
                    time.sleep(5)
                    print("Should of clicked size")
                    e.click()
            time.sleep(2)
            print("should of clicked no to sign up")
            no_button = wd.find_element_by_xpath('//*[@id="bluecoreActionScreen"]/div[2]/button')
            no_button.click()
            time.sleep(5)
            print("should of added item to cart")
            add_to_cart_button = wd.find_element_by_xpath('//*[@id="ProductDetails"]/ul/li[2]/button')
            add_to_cart_button.click()
            time.sleep(3)
            print("should of went to cart")
            go_to_cart_button = wd.find_element_by_xpath('//*[@id="app"]/div[1]/header/nav[2]/div[3]/a')
            go_to_cart_button.click()
                         
        


    def create_buy_GUI(self,userObject):
        """Creates Website GUI """
        root = Tk()
        root.geometry("700x500")
        self.buyGUI = buy_gui.BuyGUI(root,userObject)
        root.mainloop()



    def create_popup_GUI(self, message):
        """creates a pop-up GUI with given error message."""
        self.popUpGUIObject = popupGUI.PopUpGUI("None")
        self.popup_GUI_object.create_pop_up(message)