# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 11:35:44 2022

@author: Owner
"""

from tkinter import *
import tkinter as tk
from tkinter import ttk
import dashboard_controller
from database_manager import DB
from tkcalendar import DateEntry
from datetime import date, datetime
from tktimepicker import SpinTimePickerModern
from urllib.parse import urlparse
from tktimepicker import constants
from popupGUI import PopUpGUI
from urllib.request import urlopen
import buy_controller
import requests                 # pip install requests
from bs4 import BeautifulSoup   # pip install bs4



class BuyGUI():
    def __init__(self, master, userObject):
        self.dashboardControllerObject = dashboard_controller.DashboardController(userObject)
        self.buyControllerObject = buy_controller.BuyController(userObject)
        self.master = master
        self.userObject = userObject
        self.master.configure(background= "LightYellow")
        self.master.title("Buy")
        self.createMainFrame(self.userObject)

    '''
    Intent: creates the main frame for the dashboard GUI
    * Preconditions: master is connected to TKinter. 
    * createWatchlistPortfolioFrame and createSearchbarFrame have the appropriate GUI code to be called in this method.
    * Postconditions:
    * Post0. main frame for dashboard is created
    '''
    def createMainFrame(self,userObject):
        self.logo = Label(self.master, text="Scalper Bot",font='Helvetica 12',height = 6, width = 13,borderwidth=2, relief="solid", background='LightBlue1').grid(row=0,column=0, pady=5, padx=5)
        self.createButtonsFrame(userObject)
        




    '''
    Intent: creates the frame that contains the watchlist and portfolio for the dashboard GUI
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. frame that contains the watchlist and portfolio is created
    '''
    def createButtonsFrame(self,userObject):
        today = date.today()
        self.buttonFrame = Frame(self.master, width = 500, height = 300,borderwidth=2, relief="sunken", background='LightBlue1').grid(row = 2,column=1)

        self.urlLabel = Label(self.buttonFrame, text="URL:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="n")
        self.urlEntry = Entry(self.buttonFrame)
        self.urlEntry.grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="ne")
        self.dateLabel = Label(self.buttonFrame, text="Select date and time:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="e")
        d = StringVar()
        self.dateCalendar = DateEntry(self.buttonFrame, mindate=today,selectmode="day",textvariable=d).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="se")
        self.time_picker = SpinTimePickerModern(self.master)
        self.time_picker.addAll(constants.HOURS24)
        self.time_picker.configureAll(bg="#404040", height=1, fg="#ffffff", font=("Times", 16), hoverbg="#404040",
                                hovercolor="#d73333", clickedbg="#2e2d2d", clickedcolor="#d73333")
        self.time_picker.configure_separator(bg="#404040", fg="#ffffff")
        self.time_picker.grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="s")
        #self.time_picker.pack(expand=True, fill="both")
        v = IntVar()        
        w1 = "Nike"
        w2 = "Footlocker"
        w3 = "Dick's Sporting Goods"
        self.nikeButton = Radiobutton(self.buttonFrame, text = w1,value = 1, variable=v,command=lambda:v.get()).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="nw")
        self.footlockerButton = Radiobutton(self.buttonFrame, text = w2,value = 2, variable=v,command=lambda:v.get()).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="w")
        self.dicksButton = Radiobutton(self.buttonFrame, text = w3,value = 3, variable=v,command=lambda:v.get()).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="sw")
        self.closeButton = Button(self.master,text="Close",command=lambda: self.closeWindow(), background="red").grid(row = 6,column=0)
        self.nextButton = Button(self.master,text="Next",command=lambda: self.handleNextEvent(userObject,v.get(),d.get(),self.time_picker.time(),self.urlEntry.get(),self.master), background="red").grid(row = 6,column=2)
        
    def createMainFrame2(self,userObject,url,website,date,time): 
        master = Tk()
        master.geometry("550x300")
        master.title("Size and Security Code")
        self.logo = Label(master, text="Scalper Bot",font='Helvetica 12',height = 6, width = 13,borderwidth=2, relief="solid", background="LightBlue1").grid(row=0,column=0, pady=5, padx=5)
        self.closeButton = Button(master,text="Close",command=lambda: self.handleCloseEvent(), background="red").grid(row = 3,column=1)
        self.sizeLabel =  Label(master, text="Please select a size:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 1,column=1,padx=25,pady=15,ipadx=2,ipady=2)
        n = tk.StringVar()
        self.sizeChoosen = ttk.Combobox(master, width = 27, textvariable = n)
        sizeList = []
        #get size options from footlocker
        if website == "Footlocker":
           html = requests.get(url)
           soup = BeautifulSoup(html.text, 'html.parser')
           sizes = soup.find_all('button', {'class': "Button SizeSelector-button"})
           for product in sizes:
               sizeList.append(product.text)
        self.sizeChoosen['values'] = sizeList
        self.sizeChoosen.grid(row = 1,column = 2)
        self.sizeChoosen.current()
        self.securityCodeLabel = Label(master, text="Security Code:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 0,column=1,padx=25,pady=15,ipadx=2,ipady=2)
        self.securityCodeEntry = Entry(master)
        self.securityCodeEntry.grid(row = 0,column=2,padx=25,pady=15,ipadx=2,ipady=2)
        self.completeButton = Button(master,text="Complete",command=lambda: self.handleBuyEvent(website, date, time, url,master,self.securityCodeEntry.get(),self.sizeChoosen.get()), background="lightgreen").grid(row = 3,column=2)



    '''
    Intent: close the website window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the portfolio window
    '''    
    def handleNextEvent(self,userObject,buttonSelected,date,time,url,master):
        time = "{}:{}".format(*time)
        url_check = urlparse(url)
        now = datetime.now()
        month = int(now.strftime("%m"))
        day = int(now.strftime("%d"))
        buttonNetLoc = ""
        if buttonSelected == 1:
            buttonSelected = "Nike"
            buttonNetLoc ='www.nike.com'
        elif buttonSelected == 2:
            buttonSelected = "Footlocker"
            buttonNetLoc = 'www.footlocker.com'
        elif buttonSelected == 3:
            buttonSelected = "Dicks"
            buttonNetLoc ='www.dickssportinggoods.com'
        
        current_time = now.strftime("%H:%M")
        if (time <=current_time and int(date[0]) == (month) and int(date[2:4])==(day)):
            popupGUI = PopUpGUI("Too late of notice, fix the time!")
            popupGUI.createPopUp()
        elif all([url_check.scheme,url_check.netloc])==False:
            popupGUI = PopUpGUI("Incorrect URL!")
            popupGUI.createPopUp()
        elif buttonNetLoc != url_check.netloc:
            popupGUI = PopUpGUI("URL entered is not from the website selected!")
            popupGUI.createPopUp()
        else:
            self.createMainFrame2(userObject,url, buttonSelected,date, time)
            master.destroy()
            
    '''
        Intent: close the website window .
        * Preconditions: master is connected to TKinter.
        * Postconditions:
        * Post0. closes the portfolio window
    '''    
    def handleBuyEvent(self,website, date, time, url,GUI,securityCodeEntered,sizeChosen):
        self.buyControllerObject.changeBuyProcessing( website,date,time,url,GUI,securityCodeEntered,sizeChosen)


        
        
        

        

    '''
    Intent: close the payment window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the payment window
    '''    
    def closeWindow(self):
        self.master.destroy()
        self.dashboardControllerObject.createDashboardGUI()