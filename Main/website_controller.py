# -*- coding: utf-8 -*-
"""
Created on Sun Mar 27 23:35:09 2022

@author: Owner
"""

import website_gui
import dashboard_controller
import popupGUI
from database_manager import DB
from tkinter import *
import loginlogout_controller as LoginController
from user import User
from popupGUI import PopUpGUI

# this class controls the controller of the portfolio window. Its methods include 
# calculate_portfolio_value, calculate_percentage_change, call_user_object_to_remove_stock, 
# create_stock_controller_object, get_stock_price_yahoo_api_object, create_portfolio_GUI,
# CreateWatchListController, create_popup_GUI
class WebsiteController():
    def __init__(self, user_object):
        #passed in objects
        self.userObject = user_object
        self.databaseManagerObject = DB()
        self.dashboardControllerObject = dashboard_controller.DashboardController(self.userObject)
        self.websiteGUI = None
        

            



        '''
        Intent: Creates User object by passing username parameter. Returns user object
        * Preconditions: 
        * Postconditions:
        * Post0. User object created and returned.
        * Post1. User object is returned as None.
        '''
    def createUserObject(self,username):
        self.currentUserData = self.setCurrentUserData(username)
        self.currentUserItems = self.setCurrentItemData(username)
        self.userObject = User(self.currentUserData, self.currentUserItems)
        return self.userObject
    
    '''
    Intent: 
    * Preconditions: 
    * PopUpGUI exists
    * Postconditions:
    * Post0. 
    * Post1. 
    '''
    def changeWebsiteProcessing(self, websiteName, userID, websiteUsername, websitePassword, websiteGUI,username):
        """Call VerifySecurityCodeUsername if its correct reset password for user in DB.
        then creates login gui.
        Else error message pop up GUI."""
        popupGUI = PopUpGUI("The username and pasword for "+websiteName+" was saved")
        popupGUI.createPopUp()
        websiteGUI.destroy()
        check = 0

        for x in self.databaseManagerObject.getDatabaseWebsiteData():
            if websiteName in x:
                if userID in x:
                    check = 1
        print(self.databaseManagerObject.getDatabaseWebsiteData())
        if check ==1:
            self.databaseManagerObject.updateDatabaseWebsiteData(websiteName, websiteUsername, websitePassword)
        else:
            self.databaseManagerObject.insertDatabaseWebsiteData( websiteName, userID, websiteUsername, websitePassword)
        LoginController.LoginLogoutControllers().createDashboardController(username)
        
        
        
       

        

            
    def create_website_GUI(self,userObject):
        """Creates Website GUI """
        #get updated stockprice for each stock that will be displayed in GUI
        root = Tk()
        root.geometry("750x600")
        self.websiteGUI = website_gui.WebsiteGUI(root,userObject)
        root.mainloop()



    def create_popup_GUI(self, message):
        """creates a pop-up GUI with given error message."""
        self.popUpGUIObject = popupGUI.PopUpGUI("None")
        self.popup_GUI_object.create_pop_up(message)