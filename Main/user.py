import logging 
import database_manager
from utils import convert_to_type
class User():
    """User class holds login details of user as well as items associated with the user."""
    def __init__(self, current_user_data, current_user_items=None):
        self.databaseManager = database_manager.DB()
        self.current_user_data=current_user_data
        self.check_current_user_data() #checks current user data input is correct type
        #list: id:int, username:str, password:str, securitycodeanswer:int
    def __str__(self):
        return f"User Data: {self.current_user_data}"
    
    def check_current_user_data(self):
        id = self.current_user_data[0]
        username = self.current_user_data[1]
        password = self.current_user_data[2]
        # Function in utils.py
        # Checks if the variable is of the wanted type if not tries to convert it. 
        #Throws error message if it can't convert it
        convert_to_type(variable_name= "User id", variable_type= int, variable_value = id)
        convert_to_type(variable_name= "Username", variable_type= str, variable_value = username)
        convert_to_type(variable_name= "password", variable_type= str, variable_value = password)

    