from __future__ import print_function
import sys
import mysql.connector
from mysql.connector import errorcode

import os


# This class creates and maintains the Genius Finance database with methods: 
# connect_to_db, createDatabaseManager,create_database, getDatabaseUserData, 
# getDatabaseItemData, insertDatabaseUserData, insertDatabaseStockData.
class DB():
    def __init__(self):
        #creates db if necessary
        #get db name from environment 
        try:
            self.DB_NAME = str(os.getenv('DB_NAME'))
        except Exception as e:
            print("Check that MySQL database name is provided in main.py .")
            print("Oops!", sys.exc_info()[0], "occurred.")
            print("Exception: ", e)
            sys.exit(1)
        
        self.createDatabaseManager() 
        

    '''
    Intent: Connects to SQL database, returns cursor and cnx (connection) to database.
    * Cursor interacts with the MySQL server and executes operations on the database.  
    * Preconditions: myuser, mypassword, myhost (and db if given) variables to have valid values for the root 
    * user of a given MySQL server or a given database.
    * Postconditions:
    * Post0. The connection to a database db is established (if db is not None) 
    * Post1. The connection to a MySQL server is established (if db is None)
    '''
    def connect_to_db(self, db = None):
        try:
            myuser = str(os.getenv('SQLUser'))
            mypassword = str(os.getenv('SQLPassword')) 
            myhost = str(os.getenv('SQLHost'))
        except Exception as e:
            print("Check that MySQL database user, password and host are provided in main.py .")
            print("Oops!", sys.exc_info()[0], "occurred.")
            print("Exception: ", e)
        if db:
            cnx = mysql.connector.connect(
            user=myuser, 
            password=mypassword,
            host=myhost,
            database=db)
            self.cursor = cnx.cursor()
            return self.cursor, cnx            
        else:
            cnx = mysql.connector.connect(
            user=myuser, 
            password=mypassword,
            host=myhost)
            self.cursor = cnx.cursor()
            return self.cursor, cnx
        
    '''
    Intent: Creates database and tables in that database.
    * Preconditions: 
    * Connection to database is established.
    * Tables User and Stock are already formatted and ready to be created.
    * Postconditions:
    * Post0. Database is created successfully if database does not exist already.
    * Post1. Tables are created successfully if tables do not exist already.
    * Post2. Failed creating database and error is thrown if database can not be created.
    * Post3. Failed creating tables and error is thrown if tables can not be created.
    '''
    def createDatabaseManager(self):
        '''
        Intent: Creates the database 
        * Preconditions: 
        * DB_name variable is created and set to correct database name.
        * Postconditions:
        * Post0. Database GeniusFinanceDB is created successfully if no exception is thrown.
        * post1. if exception (mysql.connector.Error) is thrown, database can not created
        '''
        def create_database(cursor):
            
            try:
                cursor.execute(f"CREATE DATABASE {self.DB_NAME} DEFAULT CHARACTER SET 'utf8'")
            except mysql.connector.Error as err:
                print(f"Failed creating database: {err}")
                sys.exit(1)

        #DB_NAME = 'ScalperBotDB'
        TABLES = {}
        TABLES['User'] = (
            "CREATE TABLE `User` ("
            "  `userId` int(11) NOT NULL AUTO_INCREMENT,"
            "  `username` varchar(40)  NOT NULL,"
            "  `password` varchar(20) NOT NULL,"
            "  `securityCode` int(4) NOT NULL,"
            "  `CVV` int(4) NOT NULL,"
            "  `cardNum` varchar(20) NOT NULL,"
            "  `zipCode` int(5) NOT NULL,"
            "  `billingAddress` varchar(40) NOT NULL,"
            "  `name` varchar(40) NOT NULL,"
            "  `EXP` int(4) NOT NULL,"
            "  PRIMARY KEY (`userId`)"
            ") ENGINE=InnoDB")
        
        TABLES['Website'] = (
            "CREATE TABLE `Website` ("
            "  `websiteId` int(11) NOT NULL AUTO_INCREMENT,"
            "  `websiteName` varchar(50) NOT NULL,"
            "  `userId` int(11) NOT NULL,"
            "  `websiteUsername` varchar(40) NOT NULL,"
            "  `websitePassword` varchar(40) NOT NULL,"
            "  PRIMARY KEY (`websiteId`), FOREIGN KEY (`userId`) REFERENCES `User` (`userId`) "
            ") ENGINE=InnoDB")

        TABLES['Item'] = (
            "CREATE TABLE `Item` ("
            "  `itemId` int(11) NOT NULL AUTO_INCREMENT,"
            "  `itemName` varchar(20) NOT NULL,"
            "  `websiteId` int(11) NOT NULL,"
            "  `itemAmount` int(11) NOT NULL,"
            "  `dateOfPurchase` varchar(20) NOT NULL,"
            "  PRIMARY KEY (`itemId`), FOREIGN KEY (`websiteId`) REFERENCES `Website` (`websiteId`) "
            ") ENGINE=InnoDB")

            
        #connect to mysql server as root user
        cursor, cnx = self.connect_to_db()
       

        #check if database name already exists otherwise create it 
        try:
            cursor.execute(f"USE {self.DB_NAME}")
            
        except mysql.connector.Error as err:
            print(f"Database { self.DB_NAME} does not exists.")
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                create_database(cursor)
                print(f"Database { self.DB_NAME} created successfully.")
                cnx.database =  self.DB_NAME
                
            else:
                print(err)
                sys.exit(1)

        #specify table description for the table 
        
        for table_name in TABLES:
            table_description = TABLES[table_name]
            try:
                print("Creating table {}: ".format(table_name), end='')
                cursor.execute(table_description)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                    print("already exists.")
                
                else:
                    print(err.msg)
            else:
                print("OK")

        
        cursor.close()
        cnx.close()

   
    '''
    Intent: Query User data from database. Return a list of all User data from database
    * Preconditions: 
    * cursor is connected to correct database (GeniusFinanceDB)
    * User table already exists.
    * Postconditions:
     * Post0. Selects all data from the User table if connection to database if successful.
    * Post1. Displays None if connection to database is not successful.
    '''
    def getDatabaseUserData(self):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = ("SELECT * FROM User")
        cursor.execute(query)
        return [i for i in cursor]
    
    
    '''
    Intent: Query User data from database. Return a list of all User data from database
    * Preconditions: 
    * cursor is connected to correct database (GeniusFinanceDB)
    * User table already exists.
    * Postconditions:
     * Post0. Selects all data from the User table if connection to database if successful.
    * Post1. Displays None if connection to database is not successful.
    '''
    def getDatabaseWebsiteData(self):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = ("SELECT * FROM Website")
        cursor.execute(query)
        return [i for i in cursor]
        


    '''
    Intent: Query all Item data from database,return a list of all Item data from database. 
    * Preconditions: 
    * cursor is connected to correct database (ScalperBotDB)
    * Item table already exists.
    * Postconditions:
    * Post0. Selects all data from the Item table if connection to database if successful.
    * Post1. Displays None if connection to database is not successful.
    '''
    def getDatabaseItemData(self):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = ("SELECT * FROM Item")
        cursor.execute(query)
        return [i for i in cursor]


    '''
    Intent: Inserts data into User table
    * Preconditions: 
    * cursor is connected to correct database
    * User table already exists.
    * username, password, and securityCode are validated.
    * username and password are strings
    * securityCode is a int
    * Postconditions:
    * PostO. username, password, securityCode is inserted into the database if connection to database is successful.
    * Post1. Data is not inserted into the database if connection to database fails.
    * Post2. Data is not inserted into the database if a parameter or all parameters are equal to None.
    '''
    def insertDatabaseUserData(self, username, password, securityCode,
                               CVV, cardNum, zipCode, billingAddress, name, EXP):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = ("INSERT INTO User "
                    "(username, password, securityCode, CVV, cardNum, zipCode, billingAddress, name, EXP) "
                    "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
        data = (username, password, securityCode, CVV, cardNum, zipCode, billingAddress, name, EXP)
        cursor.execute(query, data)
        cnx.commit()


    '''
    Intent: Inserts data into Website table
    * Preconditions: 
    * userId matches with userID that is currently logged in.
    * DB_Name is equal to 'ScalperBotDB'.
    * Table that is being inserted to is "Website" and already exists.
    * cursor is connected to correct database (ScalperBotDB)
    * Postconditions:
    * PostO. websiteName, websiteUsername, and websitePassword is inserted into the database if connection to database is successful.
    * Post1. Data is not inserted into the database if connection to database fails.
    * Post2. Data is not inserted into the database if a parameter or all parameters are equal to None.
    '''
    def insertDatabaseWebsiteData(self, websiteName,userId,websiteUsername, websitePassword):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = ("INSERT INTO Website "
                    "(websiteName, userId, websiteUsername, websitePassword) "
                    "VALUES (%s, %s, %s, %s)")
        data = (websiteName, userId, websiteUsername, websitePassword)
        cursor.execute(query,data)
        cnx.commit()




    '''
    Intent: Inserts data into Item table
    * Preconditions: 
    * userId matches with userID that is currently logged in.
    * DB_Name is equal to 'ScalperBotDB'.
    * Table that is being inserted to is "Item" and already exists.
    * cursor is connected to correct database (ScalperBotDB)
    * Postconditions:
    * PostO. itemName, itemAmount, and dateOfPurchase  is inserted into the database if connection to database is successful.
    * Post1. Data is not inserted into the database if connection to database fails.
    * Post2. Data is not inserted into the database if a parameter or all parameters are equal to None.
    '''
    def insertDatabaseItemData(self, itemName,websiteId,itemAmount, dateOfPurchase):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = (f"INSERT INTO Item"
                    "(itemName, websiteId, itemAmount, dateOfPurchase) "
                    "VALUES (%s, %s, %s, %s)")
        data = (itemName, websiteId, itemAmount, dateOfPurchase)
        cursor.execute(query,data)
        cnx.commit()
        
        




    '''
    Intent: Updates data into User table
    * Preconditions: 
    * userId matches with userID that is currently logged in.
    * DB_Name is equal to 'ScalperBotDB'.
    * Table that is being updated to is "User" and already exists.
    * cursor is connected to correct database (ScalperBotDB)
    * usernameOrPassword is a string that is either equal to "username" or "password"
    * newValue is a validated username or password
    * username or password are the only valued from User table that can be changed.

    * Postconditions:
    * PostO. username is updated in the database if connection to database is successful.
    * Post1. password is updated in the database if connection to database is successful.
    * Post2. Data is not updated in the database if connection to database fails.
    * Post3. Data is not updated in the database if username or password input type is not a string
    * Post4. Data is not updated in the database if username or password is equal to None.
    '''
    def updateDatabaseUserData(self, username, usernameOrPassword, newValue):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        if usernameOrPassword == "username":
            query = (f"UPDATE User SET username = '{newValue}' WHERE username = '{username}'")
        elif usernameOrPassword == "password":
            query = (f"UPDATE User SET password = '{newValue}' WHERE username = '{username}'")
        cursor.execute(query)
        cnx.commit()
        
    '''
    Intent: Updates data into User table
    * Preconditions: 
    * userId matches with userID that is currently logged in.
    * DB_Name is equal to 'ScalperBotDB'.
    * Table that is being updated to is "User" and already exists.
    * cursor is connected to correct database (ScalperBotDB)
    * usernameOrPassword is a string that is either equal to "username" or "password"
    * newValue is a validated username or password
    * username or password are the only valued from User table that can be changed.

    * Postconditions:
    * PostO. username is updated in the database if connection to database is successful.
    * Post1. password is updated in the database if connection to database is successful.
    * Post2. Data is not updated in the database if connection to database fails.
    * Post3. Data is not updated in the database if username or password input type is not a string
    * Post4. Data is not updated in the database if username or password is equal to None.
    '''
    def updateDatabaseUserCardData(self, username, newName, newCardNum, newBillingAddress,
                                newEXP, newZipCode,newCVV):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        queryCVV = (f"UPDATE User SET CVV = '{newCVV}' WHERE username = '{username}'")
        cursor.execute(queryCVV)
        queryCardNum = (f"UPDATE User SET cardNum = '{newCardNum}' WHERE username = '{username}'")
        cursor.execute(queryCardNum)
        queryZipCode = (f"UPDATE User SET zipCode = '{newZipCode}' WHERE username = '{username}'")
        cursor.execute(queryZipCode)
        queryBillingAddress = (f"UPDATE User SET billingAddress = '{newBillingAddress}' WHERE username = '{username}'")
        cursor.execute(queryBillingAddress)
        queryName = (f"UPDATE User SET name = '{newName}' WHERE username = '{username}'")
        cursor.execute(queryName)
        queryEXP = (f"UPDATE User SET EXP = '{newEXP}' WHERE username = '{username}'")
        cursor.execute(queryEXP)
        cnx.commit()
        
    '''
    Intent: Updates data into Website table
    * Preconditions: 
    * userId matches with userID that is currently logged in.
    * DB_Name is equal to 'ScalperBotDB'.
    * Table that is being updated to is "Website" and already exists.
    * cursor is connected to correct database (ScalperBotDB)
    * websiteUsername and websitePassword are the only values from Website table that can be changed.
    * newPassword and newUsername is a validated websitePassword or websiteUsername.
    * Postconditions:
    * PostO. websiteUsername is updated in the database if connection to database is successful.
    * Post1. websitePassword is updated in the database if connection to database is successful.
    * Post2. Data is not updated in the database if connection to database fails.
    * Post3. Data is not updated in the database if websiteUsername or websitePassword input type is not a string
    * Post4. Data is not updated in the database if username or password is equal to None.
    '''
    def updateDatabaseWebsiteData(self, websiteName, newUsername, newPassword):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        queryUsername = (f"UPDATE Website SET websiteUsername = '{newUsername}'WHERE websiteName = '{websiteName}'")
        cursor.execute(queryUsername)
        queryPassword = (f"UPDATE Website SET websitePassword = '{newPassword}'WHERE websiteName = '{websiteName}'")
        cursor.execute(queryPassword)
        cnx.commit()


    '''
    Intent: Deletes data from Stock table
    * Preconditions: 
    * DB_Name is equal to 'GeniusFinanceDB'.
    * Table that is being deleted from is "Stock" and already exists.
    * cursor is connected to correct database (GeniusFinanceDB)
    * Postconditions:
    * PostO. stockName and stockOwnedAmount is inserted into the database if connection to database is successful.
    * Post1. Data is not deleted from the database if connection to database fails.
    * Post2. Data is not deleted from the database if a parameter or all parameters are equal to None.
    '''
    def deleteDatabaseWebsiteData(self, websiteName):
        cursor, cnx = self.connect_to_db(db=self.DB_NAME)
        query = (f"DELETE FROM Website WHERE websiteName = '{websiteName}'")
        cursor.execute(query)
        cnx.commit()

    