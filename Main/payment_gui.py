# -*- coding: utf-8 -*-
"""
Created on Sun Mar 27 23:35:25 2022

@author: Owner
"""

from tkinter import *
import tkinter as tk
from tkinter import ttk
import dashboard_controller
import payment_controller
from database_manager import DB


class PaymentGUI():
    def __init__(self, master, userObject):
        self.master = master
        self.master.configure(background= "LightYellow")
        self.master.title("Payment")
        self.databaseManagerObject = DB()
        self.userObject = userObject
        self.paymentControllerObject = payment_controller.PaymentController(userObject)
        self.dashboardControllerObject = dashboard_controller.DashboardController(userObject)
        self.currentUserData = None #list: id:int, username:str, password:str, securityquestionanswer:str
        #self.portfolioControllerObject = portfolio_controller.PortfolioController(userObject)
        self.createMainFrame(self.userObject)
        

    '''
    Intent: creates the main frame for the Payment GUI
    * Preconditions: master is connected to TKinter. 
    * createLoginFrame has the appropriate GUI code to be called in this method.
    * Postconditions:
    * Post0. main frame for Payment is created
    '''
    def createMainFrame(self,userObject): 
        master = self.master
        self.logo = Label(master, text="Scalper Bot",font='Helvetica 12',height = 6, width = 13,borderwidth=2, relief="solid", background="LightBlue1").grid(row=0,column=0, pady=5, padx=5)
        self.lastFourDigitsLabel = Label(master, text="****-****-****-"+userObject.current_user_data[5][-4:],font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 1,column=0,padx=30,pady=25,ipadx=5,ipady=5)
        self.closeButton = Button(master,text="Close",command=lambda: self.closeWindow(), background="red").grid(row = 6,column=1)
        self.editButton = Button(master,text="Edit",command=lambda: self.handleEditEvent(userObject), background="lightgreen").grid(row = 6,column=2)
        self.nameLabel = Label(master, text="New Name:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 0,column=1,padx=30,pady=25,ipadx=5,ipady=5)
        self.nameEntry = Entry(master)
        self.nameEntry.grid(row = 1,column=1,padx=25,pady=15,ipadx=2,ipady=2)
        self.expLabel = Label(master, text="New EXP:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row=0, column=2,padx=30,pady=25, ipadx=5,ipady=5)
        self.expEntry = Entry(master)
        self.expEntry.grid(row = 1,column=2,padx=25,pady=15,ipadx=2,ipady=2)
        self.cardNumLabel = Label(master, text="New Card Number:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row=2, column=1,padx=25,pady=25, ipadx=3,ipady=5)
        self.cardNumEntry = Entry(master)
        self.cardNumEntry.grid(row = 3,column=1,padx=25,pady=15,ipadx=2,ipady=2)
        self.zipCodeLabel = Label(master, text="New Zip Code:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 2,column=2,padx=30,pady=25,ipadx=5,ipady=5)
        self.zipCodeEntry = Entry(master)
        self.zipCodeEntry.grid(row = 3,column=2,padx=25,pady=15,ipadx=2,ipady=2)
        self.billingAddressLabel = Label(master, text="New Billing Address:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row=4, column=1,padx=30,pady=25, ipadx=5,ipady=5)
        self.billingAddressEntry = Entry(master)
        self.billingAddressEntry.grid(row = 5,column=1,padx=25,pady=15,ipadx=2,ipady=2)
        self.cvvLabel = Label(master, text="New CVV:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row=4, column=2,padx=25,pady=25, ipadx=3,ipady=5)
        self.cvvEntry = Entry(master)
        self.cvvEntry.grid(row = 5,column=2,padx=25,pady=15,ipadx=2,ipady=2)        
        
        

    '''
    Intent: close the payment window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the portfolio window
    '''    
    def handleEditEvent(self,userObject):
        self.paymentControllerObject.changePaymentProcessing(userObject.current_user_data[1], self.nameEntry.get(), self.cardNumEntry.get(), self.billingAddressEntry.get(), self.expEntry.get(), self.zipCodeEntry.get(), self.cvvEntry.get(), self.master)
        

  
    
    '''
    Intent: close the payment window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the payment window
    '''    
    def closeWindow(self):
        self.master.destroy()
        self.dashboardControllerObject.createDashboardGUI()





