from tkinter import *
import loginlogout_controller,dashboardGUI
from dashboardGUI import DashboardGUI
import user
from popupGUI import PopUpGUI
from payment_controller import PaymentController
from website_controller import WebsiteController
from buy_controller import BuyController


class DashboardController():
    """This class is the intersection of App traffic after login. """
    def __init__(self,userObject):
        self.loginlogout_controller = loginlogout_controller.LoginLogoutControllers()
        self.userObject = userObject
        self.popup_GUI_object = None
        self.payment_object = None
        self.website_object = None
        self.buy_object = None


    
    def logOutPushChanges(self, username, finalUserObject):
        """In Dashboard GUI logout button is pressed.
        The controller passes the control back to login/logout controller.
        Login logout controller function is called which pushes the changes to the database"""
        self.loginlogout_controller.logout_push_changes_to_database(username,finalUserObject)
        
    
    def create_payment_controller(self):
        """if portfolio controller object does not exist create it.
    calls method in Portfolio Controller to create Portfolio GUI."""
        if self.payment_object == None:
            self.payment_object =  PaymentController(self.userObject) 
        self.payment_object.create_payment_GUI(self.userObject)
        
        
    def create_website_controller(self):
        """if portfolio controller object does not exist create it.
    calls method in Portfolio Controller to create Portfolio GUI."""
        if self.website_object == None:
            self.website_object =  WebsiteController(self.userObject) 
        self.website_object.create_website_GUI(self.userObject)

    def create_buy_controller(self):
        """if portfolio controller object does not exist create it.
    calls method in Portfolio Controller to create Portfolio GUI."""
        if self.buy_object == None:
            self.buy_object =  BuyController(self.userObject) 
        self.buy_object.create_buy_GUI(self.userObject)



    def createDashboardGUI(self):
        """This function creates the Dashboard GUI Object"""
        root = Tk()
        root.geometry("600x400")
        self.dashboardGUIObject = DashboardGUI(root, self.userObject)
        root.mainloop()

    def openLoginGUI(self):
        """This function creates the Dashboard GUI Object"""
        self.loginlogout_controller.createLoginGUI()


