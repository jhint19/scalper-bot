# -*- coding: utf-8 -*-
"""
Created on Sun Mar 27 23:35:25 2022

@author: Owner
"""

from tkinter import *
import tkinter as tk
from tkinter import ttk
import dashboard_controller
import website_controller
from database_manager import DB
from tkinter import HORIZONTAL



class WebsiteGUI():
    def __init__(self, master, userObject):
        self.dashboardControllerObject = dashboard_controller.DashboardController(userObject)
        self.websiteControllerObject = website_controller.WebsiteController(userObject)
        self.master = master
        self.userObject = userObject
        self.master.configure(background= "LightYellow")
        self.master.title("Website")
        self.createMainFrame(self.userObject)

    '''
    Intent: creates the main frame for the dashboard GUI
    * Preconditions: master is connected to TKinter. 
    * createWatchlistPortfolioFrame and createSearchbarFrame have the appropriate GUI code to be called in this method.
    * Postconditions:
    * Post0. main frame for dashboard is created
    '''
    def createMainFrame(self,userObject):
        self.logo = Label(self.master, text="Scalper Bot",font='Helvetica 12',height = 6, width = 13,borderwidth=2, relief="solid", background='LightBlue1').grid(row=0,column=0, pady=5, padx=5)
        self.createButtonsFrame(userObject)
        




    '''
    Intent: creates the frame that contains the watchlist and portfolio for the dashboard GUI
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. frame that contains the watchlist and portfolio is created
    '''
    def createButtonsFrame(self,userObject):

        self.buttonFrame = Frame(self.master, width = 500, height = 300,borderwidth=2, relief="sunken", background='LightBlue1').grid(row = 2,column=1)

        self.usernameLabel = Label(self.buttonFrame, text="Username:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="n")
        self.usernameEntry = Entry(self.buttonFrame)
        self.usernameEntry.grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="ne")
        self.passwordLabel = Label(self.buttonFrame, text="Password:",font='Helvetica 13 bold',borderwidth=1, relief="ridge", background="white").grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="s")
        self.passwordEntry = Entry(self.buttonFrame)
        self.passwordEntry.grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="se")
        v = IntVar()        
        w1 = "Nike"
        w2 = "Footlocker"
        w3 = "Dick's Sporting Goods"
        self.nikeButton = Radiobutton(self.buttonFrame, text = w1,value = 1, variable=v,command=lambda:v.get()).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="nw")
        self.footlockerButton = Radiobutton(self.buttonFrame, text = w2,value = 2, variable=v,command=lambda:v.get()).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="w")
        self.dicksButton = Radiobutton(self.buttonFrame, text = w3,value = 3, variable=v,command=lambda:v.get()).grid(row = 2,column=1,padx=25,pady=15,ipadx=2,ipady=2,sticky="sw")
        self.closeButton = Button(self.master,text="Close",command=lambda: self.closeWindow(), background="red").grid(row = 6,column=0)
        self.saveButton = Button(self.master,text="Save",command=lambda: self.handleSaveEvent(userObject,v.get(),self.usernameEntry.get(),self.passwordEntry.get()), background="red").grid(row = 6,column=2)




    '''
    Intent: close the website window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the portfolio window
    '''    
    def handleSaveEvent(self,userObject,buttonSelected,wUsername,wPassword):
        websiteName = ""
        if buttonSelected == 1:
            websiteName = "Nike"
        elif buttonSelected == 2:
            websiteName = "Footlocker"
        elif buttonSelected == 3:
            websiteName = "Dicks"
        self.websiteControllerObject.changeWebsiteProcessing(websiteName,userObject.current_user_data[0], wUsername,wPassword,self.master,userObject.current_user_data[1])
        
        

    '''
    Intent: close the payment window .
    * Preconditions: master is connected to TKinter.
    * Postconditions:
    * Post0. closes the payment window
    '''    
    def closeWindow(self):
        self.master.destroy()
        self.dashboardControllerObject.createDashboardGUI()

        
    


