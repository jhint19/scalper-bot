# -*- coding: utf-8 -*-
"""
Created on Sun Mar 27 23:35:09 2022

@author: Owner
"""

import payment_gui
import dashboard_controller
import popupGUI
from database_manager import DB
from tkinter import *
import loginlogout_controller as LoginController
from user import User
from popupGUI import PopUpGUI

# this class controls the controller of the portfolio window. Its methods include 
# calculate_portfolio_value, calculate_percentage_change, call_user_object_to_remove_stock, 
# create_stock_controller_object, get_stock_price_yahoo_api_object, create_portfolio_GUI,
# CreateWatchListController, create_popup_GUI
class PaymentController():
    def __init__(self, user_object):
        #passed in objects
        self.userObject = user_object
        self.databaseManagerObject = DB()
        self.dashboardControllerObject = dashboard_controller.DashboardController(self.userObject)
        self.paymentGUI = None
        

            



        '''
        Intent: Creates User object by passing username parameter. Returns user object
        * Preconditions: 
        * Postconditions:
        * Post0. User object created and returned.
        * Post1. User object is returned as None.
        '''
    def createUserObject(self,username):
        self.currentUserData = self.setCurrentUserData(username)
        self.currentUserItems = self.setCurrentItemData(username)
        self.userObject = User(self.currentUserData, self.currentUserItems)
        return self.userObject
    
    '''
    Intent: 
    * Preconditions: 
    * PopUpGUI exists
    * Postconditions:
    * Post0. 
    * Post1. 
    '''
    def changePaymentProcessing(self, username, newName, newCardNum, newBillingAddress,
                                newEXP, newZipCode,newCVV, paymentGUI):
        """Call VerifySecurityCodeUsername if its correct reset password for user in DB.
        then creates login gui.
        Else error message pop up GUI."""
        chars = '1234567890'
        if len(newName)==0:
            popupGUI = PopUpGUI("Name field is blank!")
            popupGUI.createPopUp()
        elif len(newCardNum)==0:
            popupGUI = PopUpGUI("Card number field is blank!")
            popupGUI.createPopUp()
        elif len(newBillingAddress)==0:
            popupGUI = PopUpGUI("Billing address field is blank!")
            popupGUI.createPopUp()
        elif len(newEXP)==0:
            popupGUI = PopUpGUI("EXP field is blank!")
            popupGUI.createPopUp()
        elif len(newZipCode)==0:
            popupGUI = PopUpGUI("Zip code field is blank!")
            popupGUI.createPopUp()
        elif len(newCVV)==0:
            popupGUI = PopUpGUI("CVV field is blank!")
            popupGUI.createPopUp()
        elif len(newEXP)>4:
            popupGUI = PopUpGUI("EXP field is needs to be four digits, month followed by year!")
            popupGUI.createPopUp()
        elif len(newCVV)>4:
            popupGUI = PopUpGUI("CVV field is cannot exceed four digits!")
        elif len(newZipCode)>6:
            popupGUI = PopUpGUI("CVV field is cannot exceed five digits!")
            popupGUI.createPopUp()
        elif any ((c not in chars)for c in newEXP):
                popupGUI = PopUpGUI("EXP field is must be integers only!")
                popupGUI.createPopUp()
        elif any ((c not in chars)for c in newZipCode):
                popupGUI = PopUpGUI("Zip Code field is must be integers only!")
                popupGUI.createPopUp()
        elif any ((c not in chars)for c in newCVV):
                popupGUI = PopUpGUI("CVV field is must be integers only!")
                popupGUI.createPopUp()
        else:
            paymentGUI.destroy()
            self.databaseManagerObject.updateDatabaseUserCardData( username, newName, newCardNum, newBillingAddress,
                                    newEXP, newZipCode,newCVV )
            LoginController.LoginLogoutControllers().createDashboardController(username)
       

        

            
    def create_payment_GUI(self,userObject):
        """Creates Payment GUI """
        #get updated stockprice for each stock that will be displayed in GUI
        root = Tk()
        root.geometry("750x600")
        self.paymentGUI = payment_gui.PaymentGUI(root,userObject)
        root.mainloop()



    def create_popup_GUI(self, message):
        """creates a pop-up GUI with given error message."""
        self.popUpGUIObject = popupGUI.PopUpGUI("None")
        self.popup_GUI_object.create_pop_up(message)

